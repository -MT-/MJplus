package com.jplus.job.quartz;

import java.util.Set;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.core.bean.annotation.Component;
import com.jplus.core.core.ConfigHandle;
import com.jplus.job.quartz.annotation.JTask;
import com.jplus.job.quartz.bean.CronTriggerDto;
import com.jplus.job.quartz.bean.JobDetailDto;
import com.jplus.job.quartz.bean.SchedulerDto;

/**
 * 项目启动成功后执行
 * 
 * @author Yuanqy
 *
 */
@Component(initMethod = "init", destroyMethod = "destroy")
public class QuartzLoad {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	private Scheduler scheduler;

	public void init() {
		Set<Class<?>> set = QuartzPlugin.getJobSet();
		if (set.size() > 0) {
			try {
				SchedulerFactory sf = new StdSchedulerFactory();
				scheduler = sf.getScheduler();
				for (Class<?> cla : set) {
					JTask jt = cla.getAnnotation(JTask.class);
					if (jt != null) {
						SchedulerDto dto = new SchedulerDto();
						dto.setJobdetail(new JobDetailDto(jt.jobName(), jt.jobGroup(), cla));
						dto.setTrigger(new CronTriggerDto(jt.triggerName(), jt.triggerGroup(), jt.cron()));
						logger.info("==Quartz:  [{}]{}", ConfigHandle.getString(jt.cron()), cla.getName());
						addJob(dto, scheduler);
					}
				}
				// 执行启动
				scheduler.start();
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

	/**
	 * 清除(删除)所有调度数据
	 */
	public void destroy() {
		try {
			scheduler.clear();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 添加job
	 * 
	 * @param dto
	 * @throws SchedulerException
	 */
	@SuppressWarnings("unchecked")
	public void addJob(SchedulerDto dto, Scheduler scheduler) throws SchedulerException {
		addJob(scheduler, (Class<? extends Job>) dto.getJobdetail().getClas(), dto.getJobdetail().getName(), dto.getJobdetail().getGroup(), dto.getTrigger().getName(), dto.getTrigger().getGroup(),
				dto.getTrigger().getCron());
	}

	/**
	 * 添加 JOB
	 */
	public void addJob(Scheduler scheduler, Class<? extends Job> jobDetailCalss, String jobDetailName, String jobDetailGroup, String triggerName, String triggerGroup, String cron)
			throws SchedulerException {
		JobDetail job = JobBuilder.newJob(jobDetailCalss).withIdentity(jobDetailName, jobDetailGroup).build();
		CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(triggerName, triggerGroup).withSchedule(CronScheduleBuilder.cronSchedule(cron)).build();
		scheduler.scheduleJob(job, trigger);
	}

	/**
	 * 删除JOB
	 */
	public void delJob(SchedulerDto dto, Scheduler scheduler) throws SchedulerException {
		delJob(scheduler, dto.getJobdetail().getName(), dto.getJobdetail().getGroup());
	}

	/**
	 * 删除JOB
	 */
	public void delJob(Scheduler scheduler, String jobName, String jobGroup) throws SchedulerException {
		JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
		scheduler.deleteJob(jobKey);
	}

	/**
	 * 暂停JOB
	 */
	public void pauseJob(SchedulerDto dto, Scheduler scheduler) throws SchedulerException {
		pauseJob(scheduler, dto.getJobdetail().getName(), dto.getJobdetail().getGroup());
	}

	/**
	 * 暂停JOB
	 */
	public void pauseJob(Scheduler scheduler, String jobName, String jobGroup) throws SchedulerException {
		JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
		scheduler.pauseJob(jobKey);
	}

	/**
	 * 恢复JOB
	 */
	public void resumeJob(SchedulerDto dto, Scheduler scheduler) throws SchedulerException {
		resumeJob(scheduler, dto.getJobdetail().getName(), dto.getJobdetail().getGroup());
	}

	/**
	 * 恢复JOB
	 */
	public void resumeJob(Scheduler scheduler, String jobName, String jobGroup) throws SchedulerException {
		JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
		scheduler.resumeJob(jobKey);
	}
}
