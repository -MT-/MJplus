package com.jplus.rpc.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jplus.core.bean.annotation.Component;
import com.jplus.rpc.impl.handler.ServiceType;

/**
 * RPC 服务提供者
 * @author Yuanqy
 *
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Component
// 表明可被 框架 扫描
public @interface RpcProvider {
	/**
	 * 实现接口
	 */
	Class<?> iface();

	/**
	 * 协议类型
	 */
	ServiceType serviceType() default ServiceType.Netty;

	/**
	 * 服务版本号
	 */
	String version() default "1.0.0";

	/**
	 * 优先级 越小越优先>=1;当有一个最优先时，直接访问最优先的
	 */
	int priority() default 1;

	/**
	 * 优先级 越大越优先;当优先级一致时，按权重百分比概率走路由
	 */
	int weight() default 100;

}
