package com.jplus.rpc.bean;

import java.util.Map;

import com.jplus.core.AppConstant;
import com.jplus.core.util.JsonUtil;
import com.jplus.rpc.impl.handler.ServiceType;

/**
 * RPC节点属性
 * 
 * @author Yuanqy
 *
 */
public class ZNode {

	private String host;// 服务地址
	private int port;// 服务端口
	private int priority;// 优先级
	private int weight;// 权重
	private String version;
	private Map<String, String> filter;
	private ServiceType serviceType;// 协议类型
	private String ifaceName;// 接口类名
	private String appName = AppConstant.CONFIG.AppName.getValue();// 项目名称

	public ZNode() {
		super();
	}

	public ZNode(String host, int port, int priority, int weight, String version, ServiceType serviceType, String ifaceName) {
		super();
		this.host = host;
		this.port = port;
		this.priority = priority;
		this.weight = weight;
		this.version = version;
		this.serviceType = serviceType;
		this.ifaceName = ifaceName;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Map<String, String> getFilter() {
		return filter;
	}

	public void setFilter(Map<String, String> filter) {
		this.filter = filter;
	}

	public ServiceType getServiceType() {
		return serviceType;
	}

	public void setServiceType(ServiceType serviceType) {
		this.serviceType = serviceType;
	}

	@Override
	public String toString() {
		return JsonUtil.toJson(this);
	}

	public String getIfaceName() {
		return ifaceName;
	}

	public void setIfaceName(String ifaceName) {
		this.ifaceName = ifaceName;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

}
