package com.jplus.core.junit;

import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;

import com.jplus.core.AppConstant;
import com.jplus.core.bean.BeanHandle;
import com.jplus.core.core.CoreLoader;
import com.jplus.core.junit.annotation.ContextConfiguration;

/**
 * 使测试用例可按顺序执行
 *
 * @author huangyong
 * @author Yuanqy
 */
public class JplusJunit4Runner extends BlockJUnit4ClassRunner {

	private Class<?> clstest;

	public JplusJunit4Runner(Class<?> cls) throws InitializationError {
		// 调用父类构造器
		super(cls);
		clstest = cls;
		// 加载配置文件
		ContextConfiguration cc = cls.getAnnotation(ContextConfiguration.class);
		if (cc != null) {
			AppConstant.CONFIG.refresh(cc.locations());// 系统根目录
		}
		// 加入组件列表
		BeanHandle.beanSet.add(clstest);
	}

	@Override
	protected Statement methodInvoker(FrameworkMethod method, Object test) {
		// 设置容器对象，等待IOC
		BeanHandle.setTempBean(clstest, test);
		// 初始化 Helper 类，启动Jplus
		CoreLoader.init();
		return super.methodInvoker(method, test);
	}
}
