package com.jplus.core.mvc.handler.param;

import java.util.List;

import com.jplus.core.mvc.action.Handler;

public interface IWatcher {
	public List<Object> getParams(Handler handler) throws Exception;
}
