package com.jplus.core.mvc.bean;

import java.util.HashMap;
import java.util.Map;

/**
 * 封装视图对象
 *
 * @author huangyong
 * @author Yuanqy
 */
public class View {

	private String path; // 视图路径
	private Map<String, Object> data; // 相关数据
	private String type;

	public View() {
		forward();
	}

	public View(String path) {
		this.path = path;
		data = new HashMap<String, Object>();
		forward();
	}

	public View data(String key, Object value) {
		data.put(key, value);
		return this;
	}

	public View forward() {
		this.type = "forward";
		return this;
	}

	public View redirect() {
		this.type = "redirect";
		return this;
	}

	public boolean isRedirect() {
		return "redirect".equals(type);
	}

	public String getPath() {
		return path;
	}

	public View setPath(String path) {
		this.path = path;
		return this;
	}

	public Map<String, Object> getData() {
		return data;
	}

	public View setData(Map<String, Object> data) {
		this.data = data;
		return this;
	}

	public View add(String key, Object value) {
		if (this.data == null)
			this.data = new HashMap<String, Object>();
		this.data.put(key, value);
		return this;
	}
}
