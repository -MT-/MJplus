package com.jplus.core.mvc.action;

/**
 * 封装 Request 对象相关信息
 *
 * @author huangyong
 */
public class Requester {

	private String requestMethod;
	private String requestPath;

	public Requester(String requestMethod, String requestPath) {
		this.requestMethod = requestMethod;
		this.requestPath = requestPath;
	}

	/**
	 * 请求方式：[GET,POST,PUT,DELETE,,,,]
	 */
	public String getRequestMethod() {
		return requestMethod;
	}

	/**
	 * 获取reqeust请求 URI
	 */
	public String getRequestPath() {
		return requestPath;
	}

	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}

	public void setRequestPath(String requestPath) {
		this.requestPath = requestPath;
	}

}