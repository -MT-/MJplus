package com.jplus.core.plugin;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.core.AppConstant;
import com.jplus.core.bean.BeanHandle;
import com.jplus.core.core.ConfigHandle;
import com.jplus.core.util.ClassUtil;
import com.jplus.core.util.FormatUtil;

/**
 * 初始化插件
 *
 * @author yuanqy
 */
@SuppressWarnings("unchecked")
public class PluginHandle {

	private static Logger logger = LoggerFactory.getLogger(PluginHandle.class);
	/**
	 * 创建一个插件列表（用于存放插件实例）
	 */
	private static final List<Plugin> pluginList = new ArrayList<Plugin>();

	/**
	 * 获取 ClassScanner
	 */
	static {
		try {
			String[] clas = FormatUtil.trimAll(ConfigHandle.getString("plugin.allClass", "")).split(";");
			for (String cla : clas) {
				if (cla.length() > 0)
					AppConstant.APP_LOAD_PLUGIN.add((Class<? extends Plugin>) ClassUtil.loadClass(cla, false));
			}
			// 获取并遍历所有的插件类（实现了 Plugin 接口的类）
			for (Class<? extends Plugin> cls : AppConstant.APP_LOAD_PLUGIN) {
				if (Plugin.class.isAssignableFrom(cls)) {
					// 创建插件实例
					Plugin plugin = BeanHandle.getTempBean(cls);
					if (plugin == null) {
						plugin = BeanHandle.getBean(cls);
						if (plugin == null)
							plugin = (Plugin) cls.newInstance();
					}
					// 调用初始化方法
					plugin.init();
					// 将插件实例添加到插件列表中
					pluginList.add(plugin);
					logger.info("\t[plugin]{}", cls);
					BeanHandle.setBean(cls, plugin);
				}
			}
		} catch (Exception e) {
			throw new Error("初始化 PluginHelper 出错！", e);
		}
	}

	/**
	 * 获取所有插件
	 */
	public static List<Plugin> getPluginList() {
		return pluginList;
	}

	/**
	 * 销毁所有插件
	 */
	public static void destroy() {
		for (Plugin plugin : pluginList) {
			plugin.destroy();
		}
	}
}
