package com.jplus.core.aop.annotation;

import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jplus.core.bean.annotation.Component;

/**
 * 定义切面类
 *
 * @author huangyong
 * @author Yuanqy
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Component
// 用于被框架扫描
public @interface Aspect {

	/**
	 * 包名
	 */
	String pkg() default "";

	/**
	 * 类名
	 */
	String cls() default "";

	/**
	 * 注解
	 *
	 * @since 2.2
	 */
	Class<? extends Annotation> annotation() default Aspect.class;

}
