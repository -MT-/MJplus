package com.jplus.core.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

@SuppressWarnings("restriction")
public class Base64img {

	private static Logger logger = LoggerFactory.getLogger(Base64img.class);

	public static void main(String[] args) {
		// 测试从图片文件转换为Base64编码
		String strImg = GetImageStr("E:/config/img/word/media/image1.png");
		System.out.println(strImg);
		// 测试从Base64编码转换为图片文件
		GenerateImage(strImg, "E:/config/img/word/media/image1bak.png");
	}

	public static String GetImageStr(String imgFilePath) {// 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
		byte[] data = null;
		// 读取图片字节数组
		InputStream in = null;
		try {
			in = new FileInputStream(imgFilePath);
			data = new byte[in.available()];
			in.read(data);
			in.close();
		} catch (IOException e) {
			logger.error("Img read is Exception:", e);
		} finally {
			try {
				in.close();
			} catch (Exception e2) {
			}
		}
		// 对字节数组Base64编码
		BASE64Encoder encoder = new BASE64Encoder();
		return encoder.encode(data);// 返回Base64编码过的字节数组字符串
	}

	public static boolean GenerateImage(String imgStr, String imgFilePath) {// 对字节数组字符串进行Base64解码并生成图片
		if (imgStr == null) // 图像数据为空
			return false;
		BASE64Decoder decoder = new BASE64Decoder();
		OutputStream out = null;
		try {
			// Base64解码
			byte[] bytes = decoder.decodeBuffer(imgStr);
			for (int i = 0; i < bytes.length; ++i) {
				if (bytes[i] < 0) {// 调整异常数据
					bytes[i] += 256;
				}
			}
			// 生成jpeg图片
			out = new FileOutputStream(imgFilePath);
			out.write(bytes);
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			try {
				out.flush();
				out.close();
			} catch (Exception e2) {
			}
		}
	}
}
