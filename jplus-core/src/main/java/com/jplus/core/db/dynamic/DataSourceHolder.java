package com.jplus.core.db.dynamic;

/**
 * 当前数据源切换
 * 
 * @author Yuanqy
 *
 */
public class DataSourceHolder {

	private static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();

	public static void setDbType(String dbType) {
		contextHolder.set(dbType);
	}

	public static String getDbType() {
		return ((String) contextHolder.get());
	}

	public static void clearDbType() {
		contextHolder.remove();
	}
}