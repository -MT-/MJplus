// data:$('#yourformid').serialize(),// 你的formid
var async = true; // 异步
var ajaxbo = true;
// 同步调用Ajax
function sendRequestTB(requsetUrl, paramData, callback) {
	async = false;
	sendRequest(requsetUrl, paramData, callback);
	async = true;
}
// 异步调用Ajax
function sendRequest(requsetUrl, paramData, callback) {
	$(".alertLoad").parent().fadeIn();
	$.ajax({
		cache : false,
		type : "post",
		url : requsetUrl + "?timestamp=" + new Date().getTime(),
		data : paramData,
		traditional :true,
		async : async,
		success : function(data) {
			try {
				$(".alertLoad").parent().hide();
				callback(data);
				return true;
			} catch (e) {
				alert("解析异常，请稍后再试");
				console.error(e);
			}
		},
		complete : function(XMLHttpRequest, status) {
			$(".alertLoad").parent().hide();
			if (status != "success") {
				ajaxbo = false;
				if (status == "timeout") {
					alert("请求超时，请稍后再试");
				} else if (status == "error") {
					alert("系统异常，请稍后再试");
				}else{
					alert(status);
				}
			}
		}
	});
}

// 去除form表单 特殊字符
function baseCheckForm(form) {
	$(form).find("input").each(function() {
		var val = $.trim($(this).val());
		val = val.replace("<", "&lt;");
		val = val.replace(">", "&gt;");
		$(this).val(val);
	});
}
// 倒计时工具类
function countdown(time, AFunc, BFunc) {
	--time;
	setTimeout(function() {
		if (time > 0) {
			if (AFunc != null)
				AFunc(time);
			countdown(time, AFunc, BFunc);
		} else {
			if (BFunc != null)
				BFunc();
		}
	}, 1000);
}
// String去除空格
String.prototype.trim = function() {
	return this.replace(/(^\s*)|(\s*$)/g, "");
}
//jQuery 对象
function getLen(obj){
	return $(obj).val().trim().length;
}

// 验证邮箱
function isEmail(str) {
	var reg = /^(\w-*\.*)+@(\w-?)+(\.\w{2,})+$/;
	return reg.test(str);
}
// 验证手机
function isMobile(str) {
	var reg = /^(((1[0-9]{2}))+\d{8})$/;
	return reg.test(str);
}
// 验证电话
function isPhone(str) {
	var reg = /^0\d{2,3}-?\d{7,8}$/;
	return reg.test(str);
}