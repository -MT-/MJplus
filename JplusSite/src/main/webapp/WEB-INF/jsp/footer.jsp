<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<c:set var="v" value="${TIME}"/>
<div class="FOOTER">
	<div class="p20 mt20" style="border-top: 1px solid #CDCDCD">
		<div class="PW tc">
			©2017 Jplus框架   交流qq群:563930552
		</div>
	</div>
	
	<!-- 弹出框 -->
	<div id="Dialog" class="hide">
		<div >&nbsp;</div>
		<div >
			<div id="dialogClose"><button>×</button></div>
			<div id="dialogTop">title</div>
			<div id="dialogCent">content</div>
			<div id="dialogBot">
				<button class="btnCannel">Cannel</button>
				<button class="btnOk active">OK</button>
			</div>
		</div>
	</div>
	<!-- 登陆框 -->
	<div class="LoginForm hide">
		<div class="tc">
				<h3>加入我们</h3>
				<label>分享你的知识、经验 与 成长~</label>
		</div>
		<form action="" class="LoginPage">
			<input type="text" name="loginName" placeholder="手机号/邮箱" />
			<input type="text" name="loginPwd" placeholder="密码" />
			<input type="button" class="_green mt10" value="登陆"/>
			<label>我没账号，先去<a onclick="javascript:$('.LoginPage').hide();$('.RegisterPage').fadeIn();">&gt;注册&lt;</a></label>
		</form>
		<form action="" class="RegisterPage hide"> 
			<input type="text" name="" placeholder="姓名"/>
			<input type="text" name="loginName" placeholder="手机号/邮箱" />
			<input type="text" name="loginPwd" placeholder="密码" />
			<input type="button" class="_blue mt10" value="注册"/>
			<label>我有账号，直接<a onclick="javascript:$('.RegisterPage').hide();$('.LoginPage').fadeIn();">&gt;登录&lt;</a></label>
		</form>
	</div>
	<!--  -->
</div> 
<!-- footer -->
<script type="text/javascript">

</script>
</body>
</html>