<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<c:set var="v" value="${TIME}"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Jplus官网 </title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="${ctx}/resource/css/base.css?v=${v}" rel="stylesheet" type="text/css" />
<link href="${ctx}/resource/css/grid.css?v=${v}" rel="stylesheet" type="text/css" />
<link href="${ctx}/resource/css/public.css?v=${v}" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="${ctx}/resource/js/jquery.min.js?v=${v}"></script>
<script type="text/javascript" src="${ctx}/resource/js/common.js?v=${v}"></script>
<script type="text/javascript" src="${ctx}/resource/js/base.js?v=${v}"></script>
<style type="text/css">
	.PW{max-width: 1230px;min-width:1024px; margin: auto;}
	*{color:#666666;}
	@keyframes animatedBird {
		0% { background-position: 0% 0%; }
		100% { background-position: 100% 100%;  } 
	}
	.CENTER{background: url("${ctx}/resource/img/bgimg.png") center;height: 320px;animation: animatedBird 30s linear infinite;}
	.BODY a,.FOOTER a{color: #14B1D1;}
/* 	--- */
</style>
</head>
<body>
<!-- header -->
	<div class="pb10" > 
		<div class="Panel PW pt10 clear " > 
<%-- 			<img alt="" src="${ctx}/resource/img/title.png" style="height: 40px;position: relative;top: -10px;"> --%>
			<h1 class="fl" style="position: relative;margin-top: 0px">>Jplus<span id="WR"></span></h1>
			<ul class="menu fr">
				<li></li>
				<li></li>
				<li><a href="${ctx}/nav/home" ${tab==1?"class='active'":""} >首页</a></li>
				<li><a href="${ctx}/nav/doc" ${tab==2?"class='active'":""}>文档中心</a></li>
				<li><a href="#">文章集</a></li>
				<li><button onclick="showAlert({msgType:'jquery',msg:'.LoginForm',btnCannelVal:'',btnOkVal:''})">加入我们</button> </li>
			</ul>
		</div>
	</div>
<!--  body  -->