<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<c:set var="v" value="${TIME}"/>
<!-- header -->
<%@include file="../head.jsp"%>
<!-- body -->
	<div class="CENTER" >
		<div class="PW">
			<div class="g-7-24" >
				<h3 class="mt30 tc">如何评价Jplus~</h3>
				<ul class="f14  mt20">
					<li>1.支持零配置，所有配置可由.properties或代码管理。</li>
					<li>2.提供可以媲美Spring的 IOC、AOP、MVC。</li>
					<li>3.提供各种强大的第三方定制插件。</li>
					<li>4.快速开发，框架~200Kb,所有插件可插拔。</li>
				</ul>
				<div class="mt30 tc">
					<button class="active">开始Jplus之旅~</button>
					<button>加入我们</button>		
				</div>
			</div>
			<div class="g-17-24 " >
				<div class="mt10" style="background-color: white;border-radius:10px; box-shadow: 0px 5px 10px #CDCDCD;overflow: hidden;border: 1px solid #DCDCDC;">
				<table class="tsp" style="border:0px;width: 102%">
			 		<tr><th colspan="2" class="tc"><label>Jplus框架组成</label></th></tr>
			 		<tr><td><a href="http://jplus.site/cdn/jplus-core.html">jplus-core</a></td><td>jplus框架核心，集成MVC,AOP,IOC，单元测试，JdbcTemplate，后端校验，Restful请求，事务管理，多数据源支持  ...</td></tr>
			 		<tr><td><a href="http://jplus.site/cdn/jplus-orm.html">jplus-orm</a></td><td>jplus数据库框架，集成Mybatis框架,支持C3P0,Druid等多种连接池,提供分页插件,sql日志跟踪~</td></tr>
			 		<tr><td><a href="http://jplus.site/cdn/jplus-rpc.html">jplus-rpc</a></td><td>基于netty4+zookeeper,支持[netty,scokey,http[跨语言]]三种通讯协议；提供多种权重算法，支持服务调用负载均衡~</td></tr>
			 		<tr><td><a href="http://jplus.site/cdn/jplus-j2cache.html">jplus-j2cache</a></td><td>jplus二级缓存框架，集成echche+redis，拥有数据同步，保证分布式应用数据一致性~</td></tr>
			 		<tr><td><a href="http://jplus.site/cdn/jplus-job.html">jplus-job</a></td><td>jplus作业框架，集成Quartz，拥有动态作业配置功能，及其作业监控~</td></tr>
			 		<tr><td><a href="http://jplus.site/cdn/jplus-mq.html">jplus-mq</a></td><td>jplus消息框架，集成ActiveMq-jms~</td></tr>
			 	</table>
				</div>
			</div>
		</div>
	</div>
	<div class="BODY clear" >
		<div class="PW pt10"> 
			<div class="g-18-24">
				<div class="Title"><label>用户评论</label></div>
				<div class="Context" style="max-height: 600px">
					<ul class="list">
						<li style="padding-left:70px">
							<img alt="" src="${ctx}/resource/img/txdef.png" style="position: absolute;margin-left: -70px;width: 60px; height:60px; border-radius:10px; ">
							<div class=" clear"><label> Jplus:</label> <ul class="menu fr">
								<li><label>2017年1月3日 22:49:34</label></li>
								<li><a>点赞(0)</a></li>
								<li><a>回复</a></li>
							</ul></div>
							<div class="tl">这个问题其实是只可意会不可言传，然而从看到这个邀请，就有跟知友卖弄的冲动。
							这个问题其实是只可意会不可言传，然而从看到这个邀请，就有跟知友卖弄的冲动。
							这个问题其实是只可意会不可言传，然而从看到这个邀请，就有跟知友卖弄的冲动。
							</div>
						</li>
					</ul>
				</div>
				<div class="Title"><label>我有话说</label></div>
				<div class="tr mt10">
					<textarea rows="4" cols="" placeholder="我有话说~"></textarea>
					<button class="active" style="position: absolute;margin-top: -40px;margin-left: -60px">提交</button>
				</div>
			</div>
			<div class="g-6-24">
				<div class="ml20">
					<div class="denglu"></div>
					<div class="Title"><label>最新下载</label></div>
					<ul class="list">
						<li><a>Jplus1.0.0-All.zip</a> <sub>· 包含所有的框架组成集合</sub></li>
						<li><a>JplusDemo.war</a> <sub>· jplus最新的测试demo</sub></li>
					</ul>
					<div class="Title mt10"><label>热门分享</label></div>
					<ul class="list">
						<li><a>如何动手写出一个ThreadLocal</a></li>
						<li><a>如何动手写出一个线程池</a></li>
					</ul>
				</div>
			</div>
		</div>
	
	</div>
<!--  footer -->
<script type="text/javascript">
	$(function(){
		$(".denglu").append($(".LoginForm").html());
	});
</script>
<%@include file="../footer.jsp"%>