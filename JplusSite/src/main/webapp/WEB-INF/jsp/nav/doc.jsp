<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<c:set var="v" value="${TIME}"/>
<!-- header -->
<%@include file="../head.jsp"%>
<!-- body -->
	<div class="BODY clear">
		<div class="PW pt10 clear" > 
			<div class="g-5-24" >
				<div class="Title"><label>文档列表</label></div>
				<ul class="list">
					<li>Jplus-core
						<ul class="list">
							<li>声明</li>
							<li>关于Jplus</li>
							<li>Jplus简介</li>
							<li>快速创建一个jplusMvc项目</li>
							<li>关于JplusMvc的相关配置</li>
							<li>关于JplusAop的相关配置</li>
							<li>关于JplusIOC的相关配置</li>
							<li>关于Jplus Junit的相关配置</li>
							<li>关于JplusMVC的后端参数验证</li>
							<li>附加：关于Jplus的JPEL表达式的使用</li>
							<li>附加：关于Jplus多数据源的配置</li>
						</ul>
					</li>
					<li>Jplus-orm</li>
					<li>Jplus-j2cache</li>
					<li>Jplus-rpc</li>
					<li>Jplus-job</li>
					<li>Jplus-mq</li>
				</ul>
			</div>
			<div class="g-19-24" style="float: right;">
				<div class="ml10">
					<iframe src="${ctx}/resource/doc/jplus-core.html"  id="iframepage" frameborder="0" scrolling="no" width="100%" marginheight="0" marginwidth="0" onLoad="iFrameHeight()"></iframe>
					<script type="text/javascript" language="javascript">
						function iFrameHeight() {
							var ifm = document.getElementById("iframepage");
							var subWeb = document.frames ? document.frames["iframepage"].document
									: ifm.contentDocument;
							if (ifm != null && subWeb != null) {
								ifm.height = subWeb.body.scrollHeight;
							}
						}
					</script>
				</div>
			</div>
		</div>
	</div>
<!--  footer -->
<%@include file="../footer.jsp"%>