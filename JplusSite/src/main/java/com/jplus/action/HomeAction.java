package com.jplus.action;

import com.jplus.core.mvc.annotation.Controller;
import com.jplus.core.mvc.annotation.Request;
import com.jplus.core.mvc.bean.View;

@Controller
@Request("/nav/")
public class HomeAction {

	@Request.All("home")
	public View home() {
		View v = new View();
		v.add("tab", 1);
		return v;
	}

	@Request.All("doc")
	public View doc() {
		View v = new View();
		v.add("tab", 2);
		return v;
	}

}
