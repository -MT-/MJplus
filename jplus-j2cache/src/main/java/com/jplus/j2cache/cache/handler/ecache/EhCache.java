
package com.jplus.j2cache.cache.handler.ecache;

import java.util.List;

import com.jplus.j2cache.cache.iface.Cache;
import com.jplus.j2cache.cache.listener.CacheListener;
import com.jplus.j2cache.fault.CacheException;

import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.event.CacheEventListener;

/**
 * EHCache
 */
public class EhCache implements Cache, CacheEventListener {

	private net.sf.ehcache.Cache cache;
	private CacheListener listener;

	public EhCache(net.sf.ehcache.Cache cache, CacheListener listener) {
		this.cache = cache;
		this.cache.getCacheEventNotificationService().registerListener(this);
		this.listener = listener;
	}

	@SuppressWarnings("rawtypes")
	public List keys() throws CacheException {
		return this.cache.getKeys();
	}

	/**
	 * Gets a value of an element which matches the given key.
	 *
	 */
	public Object get(Object key) throws CacheException {
		try {
			if (key == null)
				return null;
			else {
				Element element = cache.get(key);
				if (element != null)
					return element.getObjectValue();
			}
			return null;
		} catch (net.sf.ehcache.CacheException e) {
			throw new CacheException(e);
		}
	}

	/**
	 * Puts an object into the cache.
	 *
	 */
	public void update(Object key, Object value) throws CacheException {
		put(key, value);
	}

	/**
	 * Puts an object into the cache.
	 */
	public void put(Object key, Object value) throws CacheException {
		try {
			Element element = new Element(key, value);
			cache.put(element);
		} catch (IllegalArgumentException e) {
			throw new CacheException(e);
		} catch (IllegalStateException e) {
			throw new CacheException(e);
		} catch (net.sf.ehcache.CacheException e) {
			throw new CacheException(e);
		}

	}

	/**
	 * Removes the element which matches the key If no element matches, nothing
	 * is removed and no Exception is thrown.
	 */
	@Override
	public void evict(Object key) throws CacheException {
		try {
			cache.remove(key);
		} catch (IllegalStateException e) {
			throw new CacheException(e);
		} catch (net.sf.ehcache.CacheException e) {
			throw new CacheException(e);
		}
	}

	@Override
	@SuppressWarnings("rawtypes")
	public void evict(List keys) throws CacheException {
		cache.removeAll(keys);
	}

	/**
	 * Remove all elements in the cache, but leave the cache in a useable state.
	 */
	public void clear() throws CacheException {
		try {
			cache.removeAll();
		} catch (IllegalStateException e) {
			throw new CacheException(e);
		} catch (net.sf.ehcache.CacheException e) {
			throw new CacheException(e);
		}
	}

	/**
	 * Remove the cache and make it unuseable.
	 */
	public void destroy() throws CacheException {
		try {
			cache.getCacheManager().removeCache(cache.getName());
		} catch (IllegalStateException e) {
			throw new CacheException(e);
		} catch (net.sf.ehcache.CacheException e) {
			throw new CacheException(e);
		}
	}

	public Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}

	@Override
	public void dispose() {
	}

	@Override
	public void notifyElementEvicted(Ehcache arg0, Element arg1) {
	}

	/**
	 * 一级缓存已被清除，所以要通知二级缓存也执行清除
	 */
	@Override
	public void notifyElementExpired(Ehcache cache, Element elem) {
		if (listener != null) {
			listener.notifyElementExpired(LEVEL_2, cache.getName(), elem.getObjectKey());
		}
	}

	@Override
	public void notifyElementPut(Ehcache arg0, Element arg1) throws net.sf.ehcache.CacheException {
	}

	@Override
	public void notifyElementRemoved(Ehcache arg0, Element arg1) throws net.sf.ehcache.CacheException {
	}

	@Override
	public void notifyElementUpdated(Ehcache arg0, Element arg1) throws net.sf.ehcache.CacheException {
	}

	@Override
	public void notifyRemoveAll(Ehcache arg0) {
	}

	@Override
	public void sendEvictCmd(String region, Object key) throws CacheException {
		// TODO Auto-generated method stub
		
	}
 

}