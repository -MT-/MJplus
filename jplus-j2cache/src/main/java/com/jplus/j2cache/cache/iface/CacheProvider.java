package com.jplus.j2cache.cache.iface;

import java.util.Properties;

import com.jplus.j2cache.cache.listener.CacheListener;
import com.jplus.j2cache.fault.CacheException;

/**
 * Support for pluggable caches.
 * 
 * @author liudong
 */
public interface CacheProvider {

	/**
	 * 缓存的标识名称
	 */
	public String name();

	/**
	 * Configure the cache
	 */
	public Cache buildCache(String regionName, boolean autoCreate, CacheListener listener) throws CacheException;

	/**
	 * Callback to perform any necessary initialization of the underlying cache
	 * implementation during SessionFactory construction.
	 *
	 */
	public void start(Properties props) throws CacheException;

	/**
	 * Callback to perform any necessary cleanup of the underlying cache
	 * implementation during SessionFactory.close().
	 */
	public void stop();

}
