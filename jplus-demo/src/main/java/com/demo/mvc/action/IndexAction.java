package com.demo.mvc.action;

import com.jplus.core.mvc.DataContext;
import com.jplus.core.mvc.WebUtil;
import com.jplus.core.mvc.annotation.Controller;
import com.jplus.core.mvc.annotation.Request;

@Controller
public class IndexAction {

	@Request.Prefix
	public void Prefix() {
		System.err.println("== This is Action prefix method ===========");
		DataContext.Response.put("Prefix", "This is Action prefix method:" + System.currentTimeMillis());
	}

	@Request.All("{msg}")
	public void index(String msg) {
		System.err.println("== This is real Action ===========");
		WebUtil.writeHTML(msg);
	}

	@Request.Suffix
	public void Suffix() {
		// 注意输出流关闭
		System.err.println("== This is Action suffix method ===========");
	}
}
