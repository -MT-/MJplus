package com.demo.mvc.action;

import com.jplus.core.mvc.WebUtil;
import com.jplus.core.mvc.annotation.Controller;
import com.jplus.core.mvc.annotation.Request;
import com.jplus.core.mvc.bean.Result;
import com.jplus.core.mvc.bean.View;

@Controller
@Request("hello/")
public class HelloAction {
	@Request.All("page")
	public View page() {
		System.err.println("== This is page Action ===========");
		// 跳转到 : /WEB-INF/jsp/hello/page.jsp
		return new View();
	}

	@Request.Get("JSON")
	public Result getJSON() {
		System.err.println("== This is page Action ===========");
		// 返回JSON : {"code":1,"obj":"请求成功"}
		return new Result().OK().DATA("请求成功");
	}

	@Request.All("restful/{msg}")
	public void restful(String msg) {
		System.err.println("== This is Restful Action ===========");
		// 使用WebUtil自定义输出
//		WebUtil.writeHTML(msg);
		WebUtil.writeTEXT(msg);
//		WebUtil.writeJSON(msg);
	}

}
