package com.demo.mvc.action;

import com.demo.mvc.bean.User;
import com.demo.mvc.iface.IIocDemo;
import com.demo.mvc.iface.impl.IocDemoImpl;
import com.jplus.core.ioc.annotation.Autowired;
import com.jplus.core.ioc.annotation.Value;
import com.jplus.core.mvc.WebUtil;
import com.jplus.core.mvc.annotation.Controller;
import com.jplus.core.mvc.annotation.Request;

@Controller
@Request("ioc/")
public class IocAction {

//	private @Autowired IIocDemo ioc1;
	private @Autowired("iocDemoImpl") IIocDemo ioc3;
	private @Autowired IocDemoImpl ioc2;

	private @Value("app.scan.pkg") String scanPkg;

	@Request.Get("test")
	private void iocTest() {
		System.out.println(ioc3.addUser(new User(1, "张三")));
		System.out.println(ioc2.getUser(1));
		System.out.println(ioc3.getUser(1));

		WebUtil.writeTEXT("OK");
	}

}
