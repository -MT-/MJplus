package com.demo.mvc.iface;

import com.demo.mvc.bean.User;
import com.jplus.core.mvc.bean.Result;

public interface IIocDemo {

	Result addUser(User user);
	Result getUser(int id);

}
