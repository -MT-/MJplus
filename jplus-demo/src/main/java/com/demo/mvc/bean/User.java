package com.demo.mvc.bean;

import com.jplus.core.mvc.annotation.Form;
import com.jplus.core.util.verify.anno.V;

@Form
public class User {

	@V.IsNotNull
	@V.IsDigits
	private int id;
	private String name;

	@V.Custom(regex = "el:{}>=18 && {}<30", retmsg = "年龄必须在18~30岁之间",isNull=false)
	private String age;
	@V.Custom(regex="^[A-Za-z]+$",retmsg="请输入正确的URL")
	private String url;
	
	@V.IsPhone
	private String phone;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User(int id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", age=" + age + ", phone=" + phone + "]";
	}

	public User() {
	}

}
