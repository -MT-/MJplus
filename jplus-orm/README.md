## Jplus-orm说明 ##  

### 声明 ###
jplus-orm是基于jplus框架的插件扩展，暂时集成了C3p0,Druid连接池和Mybatis的orm组件。加上jplus-core自带的jdbctemplete和数据源管理及事务管理。拥有如下功能：
1. 支持C3p0,Druid,mybatis,jdbcTemplate,自由选择搭配。
2. 支持事务级别控制，支持事务传播性。
3. 支持多数据源，灵活切换。
4. 全注解配置，像SSM一样方便。
5. 支持SQL日志跟踪，快速定位问题。

### 1. C3P0/Druid的配置



