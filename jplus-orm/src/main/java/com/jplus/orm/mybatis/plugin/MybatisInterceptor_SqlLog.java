package com.jplus.orm.mybatis.plugin;

import java.lang.reflect.Method;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.type.TypeHandlerRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jplus.orm.mybatis.annotation.JRemark;

/**
 * Mybatis日志记录
 * 
 * @author yuanqy
 *
 */
@Intercepts({ @Signature(type = Executor.class, method = "update", args = { MappedStatement.class, Object.class }),
		@Signature(type = Executor.class, method = "query", args = { MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class }) })
public class MybatisInterceptor_SqlLog implements Interceptor {
	private Logger log = LoggerFactory.getLogger(MybatisInterceptor_SqlLog.class);
	private static ThreadLocal<String> key_id = new ThreadLocal<String>();

	public Object intercept(Invocation invocation) throws Throwable {
		MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
		Object parameter = null;
		if (invocation.getArgs().length > 1) {
			parameter = invocation.getArgs()[1];
		}
		String sqlId = mappedStatement.getId();
		BoundSql boundSql = mappedStatement.getBoundSql(parameter);
		Configuration configuration = mappedStatement.getConfiguration();
		Object returnValue = null;
		long start = System.currentTimeMillis();
		returnValue = invocation.proceed();
		long end = System.currentTimeMillis();
		long time = (end - start);
		if (time > 1) {
			String claN = sqlId.substring(0, sqlId.lastIndexOf("."));
			String metN = sqlId.substring(sqlId.lastIndexOf(".") + 1);
			String remark = "";
			String key_type = "";
			Method[] me = Thread.currentThread().getContextClassLoader().loadClass(claN).getMethods();
			JRemark jr = null;
			for (Method method : me) {
				if (method.getName().equals(metN)) {
					jr = method.getAnnotation(JRemark.class);
					if (jr != null) {
						remark = jr.value();
						key_type = jr.key();
						break;
					}
				}
			}
			if (jr == null || jr.show() == false)
				return returnValue;
			// User u = GlobalThreadLocal.getSerUserInfo();
			log.info("==>[remark]:" + remark);
			if (jr.showSql() == true) {
				String sql = showSql(configuration, boundSql, key_type);
				log.info("==>[sql]:" + sql);
				log.info("==>[time]:" + time);
				log.debug("==>[key]:" + key_id.get());
			}
			// ================================================
		}
		return returnValue;
	}

	private static String getParameterValue(Object obj) {
		String value = null;
		if (obj instanceof String) {
			value = "'" + obj.toString() + "'";
		} else if (obj instanceof Date) {
			DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.DEFAULT, Locale.CHINA);
			value = "'" + formatter.format(new Date()) + "'";
		} else {
			if (obj != null) {
				value = obj.toString();
			} else {
				value = "";
			}
		}
		return value;
	}

	public static String showSql(Configuration configuration, BoundSql boundSql, String key_type) {
		Object parameterObject = boundSql.getParameterObject();
		List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
		String sql = boundSql.getSql().replaceAll("[\\s]+", " ");
		if (parameterMappings.size() > 0 && parameterObject != null) {
			TypeHandlerRegistry typeHandlerRegistry = configuration.getTypeHandlerRegistry();
			if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass())) {
				key_id.set(getParameterValue(parameterObject));
				sql = sql.replaceFirst("\\?", key_id.get());

			} else {
				MetaObject metaObject = configuration.newMetaObject(parameterObject);
				for (ParameterMapping parameterMapping : parameterMappings) {
					String propertyName = parameterMapping.getProperty();
					boolean bo = false;
					if (propertyName.equalsIgnoreCase(key_type))
						bo = true;
					String value = "";
					if (metaObject.hasGetter(propertyName)) {
						Object obj = metaObject.getValue(propertyName);
						value = getParameterValue(obj);
						sql = sql.replaceFirst("\\?", value);
					} else if (boundSql.hasAdditionalParameter(propertyName)) {
						Object obj = boundSql.getAdditionalParameter(propertyName);
						value = getParameterValue(obj);
						sql = sql.replaceFirst("\\?", value);
					}
					if (bo)
						key_id.set(value);
				}
			}
		}
		return sql;
	}
	@Override
	public Object plugin(Object target) {
		return Plugin.wrap(target, this);
	}

	@Override
	public void setProperties(Properties properties) {
		// TODO Auto-generated method stub
	}

	public static void main(String[] args) throws SecurityException, NoSuchMethodException, ClassNotFoundException {
		String sqlId = "com.demo.dao.ITestDao.getByName";
		String claN = sqlId.substring(0, sqlId.lastIndexOf("."));
		String metN = sqlId.substring(sqlId.lastIndexOf(".") + 1);
		System.out.println(claN + "==" + metN);

	}
}